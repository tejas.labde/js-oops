let userOne={
    email:'ryu@ninjas.com',
    name:'Ryu',
    login(){
        console.log(this.email,'has logged in');
    },
    logout(){
        console.log(this.email,'has logged out');
    }
};

let userTwo={
    email:'yoshi@ninjas.com',
    name:'Yoshi',
    login(){
        console.log(this.email,'has logged in');
    },
    logout(){
        console.log(this.email,'has logged out');
    }
};

let userThree={
    email:'lewis@ninjas.com',
    name:'Lewis',
    login(){
        console.log(this.email,'has logged in');
    },
    logout(){
        console.log(this.email,'has logged out');
    }
};
console.log(userOne.name);
userOne.name='Yoshi';
userOne.name='Ryu';

//*****************************************************88 */
// video3
userOne['email'];//to access value of key email
userOne['name']='max';
//to create a new property called age in the object
userOne.age=25;

/************************************************ */
//video5-easier way to create an object
class User{
    constructor(name,email){
        this.name=name;
        this.email=email;
    }
}
 
var uOne=new User('daniel','daniel@mcLaren.com');
var uTwo=new User('checo','checo@redbullracing.com');

//video6-class methods
class User1{
    constructor(name,email){
        this.name=name;
        this.email=email;
        this.score=0;
    }
    login(){
        console.log('this.email','just logged in');
        return this;
    }
    logout(){
        console.log('this.email','just logged out');
        return this;
    }
    updateScore(){
        this.score++;
        console.log(this.name,'score',this.score);
        return this;
    }
}

 
var uone=new User1('daniel','daniel@mcLaren.com');
var utwo=new User1('checo','checo@redbullracing.com');
uone.login();
utwo.logout();

//vid7-method chaining
uone.logout().updateScore().updateScore().login();

class Admin extends User1{
    // deleteUser(users){
    //     users=users.filter(u=>{
    //         return u.email!=user.email
    //     })
    // }
}

var admin=new Admin('toto','toto@mercedesamg.com');
// admin.deleteUser(utwo);
var users=[userOne,userTwo];

//video9-constructor under the hood
function UserConst(email,name){
    this.email=email;
    this.name=name;
    this.online=false;
    
}


//video10-prototype,this will append the login function to the prototype of the object

UserConst.prototype.login=function(){
    this.online=true;
    console.log(`${this.email} has logged in`);
}
UserConst.prototype.logout=function(){
    this.online=false;
    console.log(`${this.email} has logged out`);
}

function AdminConstruct(...args){
    console.log(args);
    User.apply(this,args);
    this.role='super admin'

}

Admin.prototype=Object.create(User.prototype);
var userA=new UserConst('daniel','daniel@mcLaren.com');
var userB=new UserConst('checo','checo@redbullracing.com');
var adm=new AdminConstruct('max','max@redbullracing.com');
